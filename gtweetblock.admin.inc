<?php

/**
 * @file
 *   gtweetblock module's form api code.
 */

function gtweetblock_settings_form() {
  $form = array();
  $form['gtweetblock_method'] = array(
    '#type'          => 'radios', 
    '#title'         => t('Choose Method'), 
    '#default_value' => variable_get('gtweetblock_method', 'localstorage'), 
    '#options'       => array('localstorage' => t('localStorage'), 'cache' => t('Cache'))
  );
  $form['gtweetblock_user'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Username'),
    '#default_value' => variable_get('gtweetblock_user', 'g_ung')
  );
  $form['gtweetblock_count'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Count'),
    '#default_value' => variable_get('gtweetblock_count', '10')
  );
  $form['gtweetblock_upinterval'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Update interval in minutes'),
    '#default_value' => variable_get('gtweetblock_upinterval', '10')
  );
  $form['submit'] = array(
    '#type'  => 'submit',
    '#value' => t('Submit')
  );
  return $form;
}