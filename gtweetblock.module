<?php

/**
 * Implements hook_help($path, $arg).
 */
function gtweetblock_help($path, $arg) {
  switch ($path) {
    case 'admin/settings/gtweetblock':
      return '<p>' . t('pure Twitter!') . '</p>';
      break;
    case 'admin/help#gtweetblock':
      return '<p>' . t('Fetches tweets from Twitter, store it either in localStorage or cache') . '</p>';
      break;
  }
}

/**
 * Implements hook_permission().
 */
function gtweetblock_permission() {
  return array(
    'administer gtweetblock' => array(
    'title'       => t('Admin gtweetblock'),
    'description' => t('configure gtweetblock'),
  ));
}

/**
 * Implements hook_menu().
 */
function gtweetblock_menu() {
  $items = array();
  $items['admin/config/gtweetblock'] = array(
    'title'            => 'gtweetblock',
    'page callback'    => 'drupal_get_form',
    'page arguments'   => array('gtweetblock_settings_form'),
    'access arguments' => array('administer gtweetblock'),
    'description'      => 'Configure gtweetblock',
    'file'             => 'gtweetblock.admin.inc',
  );
  return $items;
}

function gtweetblock_settings_form_submit($form, &$form_state) {
  variable_set('gtweetblock_user', $form_state['values']['gtweetblock_user']);
  variable_set('gtweetblock_count', $form_state['values']['gtweetblock_count']);
  variable_set('gtweetblock_upinterval', $form_state['values']['gtweetblock_upinterval']);
  variable_set('gtweetblock_method', $form_state['values']['gtweetblock_method']);
  cache_clear_all('gtweetblock', 'cache', FALSE);
  drupal_set_message(t('Saved it!'));
}

/**
 * Implements hook_block_info().
 */
function gtweetblock_block_info() {
  $blocks['gtweetblock'] = array(
    'info'       => t('gtweetblock'),
    'status'     => TRUE,
    'weight'     => 0,
    'visibility' => BLOCK_VISIBILITY_NOTLISTED,
    'region'     => 'sidebar_second',
  );
  return $blocks;
}

/**
 * Implements hook_block_view($delta = '').
 */
function gtweetblock_block_view($delta = '') {
  $block = array();
  switch ($delta):
    case 'gtweetblock':
      $block['subject'] = t('Tweets');
      $block['content'] = gtweetblock_decide(variable_get('gtweetblock_method'));
    break;
  endswitch;
  return $block;
}

/**
 * decide if using localStorage or cache.
 */
function gtweetblock_decide($method) {
  switch ($method):
    case 'localstorage':
      return gtweetblock_get_ls(variable_get('gtweetblock_upinterval'), variable_get('gtweetblock_user'), variable_get('gtweetblock_count'));
      break;
    case 'cache':
      return gtweetblock_get_c(variable_get('gtweetblock_upinterval'), variable_get('gtweetblock_user'), variable_get('gtweetblock_count'));
      break;
  endswitch;
}

// localStorage
function gtweetblock_get_ls($update, $u, $c) {
  // pass data from PHP to JS with 'setting', available in JS with 'Drupal.settings.' !!!
  $settings = array(
    'gtweetblock_data' => array(
      'c'         => $c,
      'u'         => $u,
      'updateint' => $update * 60000,
    ),
  );
  drupal_add_js($settings, 'setting');
  
  $script = drupal_get_path('module', 'gtweetblock') . '/localtweets_code.js';
  drupal_add_js($script, array('type' => 'file'));
  $build = array(
    '#type'   => 'markup',
    '#markup' => '<ul id="gtweetblock"></ul>',
  );
  return $build;
}

// cache
function gtweetblock_get_c($duration, $u, $c) {
  $cache = cache_get("gtweetblock");
  if (isset($cache->data)) {
    $c_data = $cache->data;
    return array(
      '#items' => $c_data,
      '#theme' => 'item_list',
      '#attributes' => array(
        'id' => 'gtweetblock',
      ),
    ); 
  }
  else{
    $url = "http://api.twitter.com/1/statuses/user_timeline.json?screen_name=" . $u . "&count=" . $c . "&include_rts=true";
    $rsp = drupal_http_request($url);
      if  ($rsp->data && !isset($rsp->error)) {
        $jsondec = json_decode($rsp->data);
        foreach ($jsondec as $d) {
          $tweets_data[] = theme('format_tweets', array(
            'text'       => UserDec(HashDec(LinkDec($d->text))),
            'created_at' => mydate($d->created_at),
        ));
        }
        // setting cache, so items are already formatted when coming from cache
        cache_set("gtweetblock", $tweets_data, 'cache', 60 * $duration);
        return array(
          '#items' => $tweets_data,
          '#theme' => 'item_list',
          // set id to the corresponding wrapping <ul>
          '#attributes' => array(
            'id' => 'gtweetblock',
          ),
        ); 
      }
  }
}
// END decision

/**
 * Implements theme_function($variables).
 */
function theme_format_tweets($variables) {
  // make passed variables available as $variables instead of $variables['passed_var']
  extract($variables);
  return $text . '<p id="gtweet-date">' . $created_at . '</p>';
}

/**
 * Implements hook_theme().
 */
function gtweetblock_theme() {
  return array(
    'format_tweets' => array(
      'variables' => array(
        'text'       => NULL,
        'created_at' => NULL,
      )
    )
  );
}

/**
 * Helper functions.
 */

// Date formatter
function mydate($timeStamp) {
  $now = getdate(time());
  $diff = time() - strtotime($timeStamp);
  $tweetTimestamp = '';
  if ($diff < 60)  {
    $tweetTimestamp .= $diff . ' second' . ($diff == 1 ? '' : 's') . ' ago';
  }
  elseif ($diff < 3600) {
    $t = (int)(($diff + 30) / 60);
    $tweetTimestamp .= $t . ' minute' . ($t == 1 ? '' : 's') . ' ago';
  }
  elseif ($diff < 86400) {
    $t = (int)(($diff + 1800) / 3600);
    $tweetTimestamp .= $t . ' hour' . ($t == 1 ? '' : 's') . ' ago';
  }
  else {
    $d = date_parse($timeStamp);
    //print_r($d);die();
    $period = 'AM';
    $hours = $d['hour'];
    if ($hours > 12) {
      $hours -= 12;
      $period = 'PM';
    }
    $mins = $d['minute'];
    $minutes = ($mins < 10 ? '0' : '') . $mins;
    $monthName = array( "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec" );
    $tweetTimestamp .= $monthName[$d['month']-1] . ' ' . $d['day'];
    if ($d['year'] < $now['year']) {
      $tweetTimestamp .= ', ' . $d['year'];
    }
    $t = (int)(($diff + 43200) / 86400);
    $tweetTimestamp .= ' (' . $t . ' day' . ($t == 1 ? '' : 's') . ' ago)';
  }
  return $tweetTimestamp;
}

// Link Decorator
function LinkDec($text) {
  // the regex to markup links
  return preg_replace('/((ftp|http|https):\/\/(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?)/i', '<a href="$1" class="gtweet-a gtweet-link" target="_blank" rel="nofollow">$1</a>', $text);
}

// Hash Decorator
function HashDec($text) {
  // the regex to markup #hashtags
  return preg_replace('/#([a-zA-Z0-9_]+)/i', '<a href="http://search.twitter.com/search?q=%23$1" class="gtweet-a gtweet-hashtag" title="#$1" target="_blank" rel="nofollow">#$1</a>', $text);
}

// Username Decorator
function UserDec($text) {
  // the regex to markup @usernames. if @Anywhere is present the task is left to them
  return preg_replace('/@([a-zA-Z0-9_]+)/i', '@<a href="http://twitter.com/$1" class="gtweet-a twitter-anywhere-user" target="_blank" rel="nofollow">$1</a>', $text);
}