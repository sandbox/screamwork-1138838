(function ($) {$(function(){
	
  username = Drupal.settings.gtweetblock_data.u,
  count = Drupal.settings.gtweetblock_data.c,
  updateinterval = Drupal.settings.gtweetblock_data.updateint,
  url = "http://api.twitter.com/1/statuses/user_timeline.json?screen_name=" + username + "&count=" + count + "&include_rts=true&callback=?";
  
  // browser must support localStorage
  if (localStorage) {
	if (localStorage.getItem('gtweetblock-date') && ((new Date().getTime() - localStorage.getItem('gtweetblock-date')) < updateinterval)) {
        items = new Array;
        items = JSON.parse(localStorage.getItem('gtweetblock'));
        //alert("items from storage, less than 30 sec.");
        $.each(items, function(){
          $("<li>" +  UserDec(HashDec(LinkDec(this.text))) + "<p id='gtweet-date'>" + mydate(this.created_at) + "</p></li>").appendTo("#gtweetblock");
        });  
    }
	else if(localStorage.getItem('gtweetblock-date') && (new Date().getTime() - localStorage.getItem('gtweetblock-date')) > updateinterval || !localStorage.getItem('gtweetblock-date')){
	  $.getJSON(url, function(cb){
        try {
          // remove them first 
          localStorage.getItem('gtweetblock') ? localStorage.removeItem('gtweetblock') : '';
          localStorage.getItem('gtweetblock-date') ? localStorage.removeItem('gtweetblock-date') : '';
          // now set the new one
          localStorage.setItem('gtweetblock', JSON.stringify(cb));
          localStorage.setItem('gtweetblock-date', new Date().getTime());
          //alert("new items set to storage.");
        }
        // if localStorage is full
        catch (e) {
          if (e == QUOTA_EXCEEDED_ERR) {
            alert(Drupal.t("Quota exceeded!"));
          }
        } // end catch
	    $.each(cb, function(){
	      $("<li>" + UserDec(HashDec(LinkDec(this.text))) + "<p id='gtweet-date'>" + mydate(this.created_at) + "</p></li>").appendTo("#gtweetblock");
	    });
	  }); // END getJSON
	} // END elseif
  } // END if localStorage
  else {
	  $("<li>local storage not supported!</li>").appendTo("#gtweetblock");
  }
  
// ---- Helper functions - Formatters ---- //
  
  // Date formatter
  function mydate(timeStamp) {
	var now = new Date();
	var diff = parseInt((now.getTime() - Date.parse(timeStamp)) / 1000);
	var tweetTimestamp = '';
	if (diff < 60)	{
	  tweetTimestamp += diff + ' second' + (diff == 1 ? '' : 's') + ' ago';
	}
	else if (diff < 3600) {
	  var t = parseInt((diff + 30) / 60);
	  tweetTimestamp += t + ' minute' + (t == 1 ? '' : 's') + ' ago';
	}
	else if (diff < 86400) {
	  var t = parseInt((diff + 1800) / 3600);
	  tweetTimestamp += t + ' hour' + (t == 1 ? '' : 's') + ' ago';
	}
	else {
	  var d = new Date(timeStamp);
	  var period = 'AM';
	  var hours = d.getHours();
	  if (hours > 12) {
		hours -= 12;
		period = 'PM';
	  }
	  var mins = d.getMinutes();
	  var minutes = (mins < 10 ? '0' : '') + mins;
	  var monthName = [ "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec" ];
	  tweetTimestamp += monthName[d.getMonth()] + ' ' + d.getDate();
	  if (d.getFullYear() < now.getFullYear()) {
		tweetTimestamp += ', ' + d.getFullYear();
	  }
	  var t = parseInt((diff + 43200) / 86400);
	  tweetTimestamp += ' (' + t + ' day' + (t == 1 ? '' : 's') + ' ago)';
	}
	return tweetTimestamp;
  };
  
  // Link Decorator
  function LinkDec(text) {
	// the regex to markup links
	return text.replace(/((ftp|http|https):\/\/(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?)/gi,'<a href="$1" class="gtweet-a gtweet-link" target="_blank" rel="nofollow">$1<\/a>');
  };
  
  // Hash Decorator
  function HashDec(text) {
	// the regex to markup #hashtags
	return text.replace(/#([a-zA-Z0-9_]+)/gi,'<a href="http://search.twitter.com/search?q=%23$1" class="gtweet-a gtweet-hashtag" title="#$1" target="_blank" rel="nofollow">#$1<\/a>');
  };

  // Username Decorator
  function UserDec(text) {
	// the regex to markup @usernames. if @Anywhere is present the task is left to them
	return text.replace(/@([a-zA-Z0-9_]+)/gi,'@<a href="http://twitter.com/$1" class="gtweet-a twitter-anywhere-user" target="_blank" rel="nofollow">$1<\/a>');
  };
  
})})(jQuery);